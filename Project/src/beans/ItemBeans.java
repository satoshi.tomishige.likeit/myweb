package beans;
import java.io.Serializable;

public class ItemBeans implements Serializable {
	private int id;
	private int classId;
	private int colorId;
	private int formal;
	private String brand;
	private String store;
	private String memo;
	private String photo;
	private int userId;
	private int price;
	private int useNumber;
	private String uploadDate;




	public ItemBeans(int id ,int classId, int colorId, int formal, String brand, String store, String memo, String photo,
			int userId, int price) {
		super();
		this.id = id;
		this.classId = classId;
		this.colorId = colorId;
		this.formal = formal;
		this.brand = brand;
		this.store = store;
		this.memo = memo;
		this.photo = photo;
		this.userId = userId;
		this.price = price;
	}

	public ItemBeans() {}



	public ItemBeans(int id, int classId, int colorId, int formal, String brand, String store, String memo,
			String photo, int userId, int price, int useNumber, String uploadDate) {
		super();
		this.id = id;
		this.classId = classId;
		this.colorId = colorId;
		this.formal = formal;
		this.brand = brand;
		this.store = store;
		this.memo = memo;
		this.photo = photo;
		this.userId = userId;
		this.price = price;
		this.useNumber = useNumber;
		this.uploadDate = uploadDate;
	}

	public int getClassId() {
		return classId;
	}

	public void setClasssId(int classId) {
		this.classId = classId;
	}

	public int getColorId() {
		return colorId;
	}

	public void setColorId(int colorId) {
		this.colorId = colorId;
	}

	public int getPrice() {
		return price;
	}

	public void setPrice(int price) {
		this.price = price;
	}

	public int getFormal() {
		return formal;
	}

	public void setFormal(int formal) {
		this.formal = formal;
	}

	public String getBrand() {
		return brand;
	}

	public void setBrand(String brand) {
		this.brand = brand;
	}

	public String getStore() {
		return store;
	}

	public void setStore(String store) {
		this.store = store;
	}

	public String getMemo() {
		return memo;
	}

	public void setMemo(String memo) {
		this.memo = memo;
	}

	public String getPhoto() {
		return photo;
	}

	public void setPhoto(String photo) {
		this.photo = photo;
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getUseNumber() {
		return useNumber;
	}

	public void setUseNumber(int useNumber) {
		this.useNumber = useNumber;
	}

	public String getUploadDate() {
		return uploadDate;
	}

	public void setUploadDate(String uploadDate) {
		this.uploadDate = uploadDate;
	}

}
