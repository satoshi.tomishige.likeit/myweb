package beans;

import java.io.Serializable;
import java.util.List;

import dao.CoordinateDetailDAO;

public class CoordinateBeans implements Serializable {
	private int id;
	private String photo;
	private int user_id;
	private String date;

	public CoordinateBeans(int id, String photo, int user_id, String date) {
		super();
		this.id = id;
		this.photo = photo;
		this.user_id = user_id;
		this.date = date;
	}

	public CoordinateBeans(int id, String photo, int user_id) {
		super();
		this.id = id;
		this.photo = photo;
		this.user_id = user_id;
	}

	public CoordinateBeans(String photo, int user_id) {
		super();
		this.photo = photo;
		this.user_id = user_id;
	}

	public CoordinateBeans() {
		super();
	}

	public String getPhoto() {
		return photo;
	}

	public void setPhoto(String photo) {
		this.photo = photo;
	}

	public int getUser_id() {
		return user_id;
	}

	public void setUser_id(int user_id) {
		this.user_id = user_id;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public List<ItemBeans> getDetaliItemList() {
		return CoordinateDetailDAO.findItem(this.id);

	}

}
