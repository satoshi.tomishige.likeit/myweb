package beans;

import java.io.Serializable;

public class UserDataBeans implements Serializable{
	private int id;
	private String name;
	private String loginId;
	private String password;

	public UserDataBeans() {
		this.name = "";
		this.loginId = "";
		this.password = "";
	   this.id = 0;	}

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getLoginId() {
		return loginId;
	}
	public void setLoginId(String loginId) {
		this.loginId = loginId;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}

}
