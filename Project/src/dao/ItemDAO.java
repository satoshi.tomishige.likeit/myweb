package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import base.DBManager;
import beans.ItemBeans;

public class ItemDAO {
	public static int ItemRegister(ItemBeans IB) {
		Connection conn = null;
		PreparedStatement pstmt = null;
		int result = 0;
		try {
			conn = DBManager.getConnection();
			String sql = "insert into c_item(class_id ,color_id ,formal ,brand ,store ,memo ,photo ,user_id, price,upload_date) values (?,?,?,?,?,?,?,?,?,now())";
			pstmt = conn.prepareStatement(sql);
			pstmt.setInt(1, IB.getClassId());
			pstmt.setInt(2, IB.getColorId());
			pstmt.setInt(3, IB.getFormal());
			pstmt.setString(4, IB.getBrand());
			pstmt.setString(5, IB.getStore());
			pstmt.setString(6, IB.getMemo());
			pstmt.setString(7, IB.getPhoto());
			pstmt.setInt(8, IB.getUserId());
			pstmt.setInt(9, IB.getPrice());

			result = pstmt.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();

		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();

				}
			}

		}
		return result;
	}

	public ArrayList<ItemBeans> findAllItem() {
		Connection conn = null;
		ArrayList<ItemBeans> itemList = new ArrayList<ItemBeans>();
		try {
			conn = DBManager.getConnection();
			String sql = "select * from c_item";
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(sql);

			while (rs.next()) {
				ItemBeans item = new ItemBeans(
						rs.getInt("id"),
						rs.getInt("class_id"),
						rs.getInt("color_id"),
						rs.getInt("formal"),
						rs.getString("brand"),
						rs.getString("store"),
						rs.getString("memo"),
						rs.getString("photo"),
						rs.getInt("user_id"),
						rs.getInt("price"));
				itemList.add(item);

			}
		} catch (SQLException e) {
			e.printStackTrace();

		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();

				}
			}

		}
		return itemList;
	}

	public static ArrayList<ItemBeans> findMyItem(int userId, int classId) {
		Connection conn = null;
		ArrayList<ItemBeans> itemList = new ArrayList<ItemBeans>();
		try {
			conn = DBManager.getConnection();
			String sql = "select * from c_item where user_id = ? and class_id = ?";
			PreparedStatement stmt = conn.prepareStatement(sql);
			stmt.setInt(1, userId);
			stmt.setInt(2, classId);
			ResultSet rs = stmt.executeQuery();

			while (rs.next()) {
				ItemBeans item = new ItemBeans(
						rs.getInt("id"),
						rs.getInt("class_id"),
						rs.getInt("color_id"),
						rs.getInt("formal"),
						rs.getString("brand"),
						rs.getString("store"),
						rs.getString("memo"),
						rs.getString("photo"),
						rs.getInt("user_id"),
						rs.getInt("price"));
				itemList.add(item);

			}
		} catch (SQLException e) {
			e.printStackTrace();

		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();

				}
			}

		}
		return itemList;
	}

	public ArrayList<ItemBeans> findItemById(int userId) {
		Connection conn = null;
		ArrayList<ItemBeans> itemList = new ArrayList<ItemBeans>();
		try {
			conn = DBManager.getConnection();
			String sql = "select * from c_item where user_id = ?";
			PreparedStatement stmt = conn.prepareStatement(sql);
			stmt.setInt(1, userId);
			ResultSet rs = stmt.executeQuery();

			while (rs.next()) {
				ItemBeans item = new ItemBeans(
						rs.getInt("id"),
						rs.getInt("class_id"),
						rs.getInt("color_id"),
						rs.getInt("formal"),
						rs.getString("brand"),
						rs.getString("store"),
						rs.getString("memo"),
						rs.getString("photo"),
						rs.getInt("user_id"),
						rs.getInt("price"));
				itemList.add(item);

			}
		} catch (SQLException e) {
			e.printStackTrace();

		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();

				}
			}

		}
		return itemList;
	}

	public ItemBeans findItem(int itemId) {
		Connection conn = null;
		ItemBeans IB = new ItemBeans();
		try {
			conn = DBManager.getConnection();
			String sql = "select * from c_item where id=?";
			PreparedStatement stmt = conn.prepareStatement(sql);
			stmt.setInt(1, itemId);

			ResultSet rs = stmt.executeQuery();

			while (rs.next()) {
				IB = new ItemBeans(
						rs.getInt("id"),
						rs.getInt("class_id"),
						rs.getInt("color_id"),
						rs.getInt("formal"),
						rs.getString("brand"),
						rs.getString("store"),
						rs.getString("memo"),
						rs.getString("photo"),
						rs.getInt("user_id"),
						rs.getInt("price"),
						rs.getInt("use_number"),
						rs.getString("upload_date"));

			}
		} catch (SQLException e) {
			e.printStackTrace();

		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();

				}
			}

		}
		return IB;
	}

	public int addUse(String[] itemList) {
		Connection conn = null;
		int rs = 0;
		try {
			conn = DBManager.getConnection();

			for (String ItemId : itemList) {
				String sql = "update c_item set use_number =use_number+1 where id=?";
				PreparedStatement pstmt = conn.prepareStatement(sql);
				pstmt.setString(1, ItemId);
				rs = pstmt.executeUpdate();
			}
		} catch (SQLException e) {
			e.printStackTrace();

		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();

				}
			}

		}

		return rs;

	}

	public List<ItemBeans> findSortedItem(int userId) {
		Connection conn = null;
		ItemBeans IB = new ItemBeans();
		ArrayList<ItemBeans> list = new ArrayList<ItemBeans>();
		try {
			conn = DBManager.getConnection();
			String sql = "select *,(price / use_number) as cost from c_item where user_id=? order by cost asc limit 8";
			PreparedStatement pstmt = conn.prepareStatement(sql);
			pstmt.setInt(1, userId);
			ResultSet rs = pstmt.executeQuery();
			while (rs.next()) {
				IB = new ItemBeans(
						rs.getInt("id"),
						rs.getInt("class_id"),
						rs.getInt("color_id"),
						rs.getInt("formal"),
						rs.getString("brand"),
						rs.getString("store"),
						rs.getString("memo"),
						rs.getString("photo"),
						rs.getInt("user_id"),
						rs.getInt("price"),
						rs.getInt("use_number"),
						rs.getString("upload_date"));
				list.add(IB);
			}
		} catch (SQLException e) {
			e.printStackTrace();

		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();

				}
			}

		}

		// TODO 自動生成されたメソッド・スタブ
		return list;
	}

	public int remove(int itemId) {
		Connection conn = null;
		int rs = 0;
		try {
			conn = DBManager.getConnection();
			String sql = "delete from c_item where id=?";
			PreparedStatement pstmt = conn.prepareStatement(sql);
			pstmt.setInt(1, itemId);
			 rs = pstmt.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();

		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();

				}
			}

		}

		// TODO 自動生成されたメソッド・スタブ
		return rs;
	}
}