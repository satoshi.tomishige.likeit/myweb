package dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import base.DBManager;
import beans.ItemColorBeans;

public class ItemColorDAO {
	public  ArrayList<ItemColorBeans> findAllColor() {
		Connection conn = null;
		ArrayList<ItemColorBeans> list = new ArrayList<ItemColorBeans>();
		try {
			conn = DBManager.getConnection();

			Statement stmt = conn.createStatement();

			String sql = "select * from c_color";

			ResultSet rs = stmt.executeQuery(sql);

			while (rs.next()) {
				ItemColorBeans ICB = new ItemColorBeans();
				ICB.setId(rs.getInt("id"));
				ICB.setName(rs.getString("name"));
				ICB.setCode(rs.getString("code"));
				list.add(ICB);
			}
		} catch (SQLException e) {
			e.printStackTrace();

		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();

				}
			}

		}return list;
	}

}


