package dao;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.servlet.annotation.WebServlet;
import javax.xml.bind.DatatypeConverter;

import base.DBManager;
import beans.UserDataBeans;

/**
 * Servlet implementation class UserDAO
 */
@WebServlet("/UserDAO")
public class UserDAO {
	public int findByLoginInfo(String loginId, String password) {
		Connection conn = null;
		int userId = 0;
		String passkey = passKey(password);
		try {
			conn = DBManager.getConnection();
			String sql = "select * from c_user where login_id=? and password=?";
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, loginId);
			pStmt.setString(2, passkey);
			ResultSet rs = pStmt.executeQuery();
			if(!rs.next()) {
				return 0;
			}
			userId = rs.getInt("id");

		} catch (SQLException e) {
			e.printStackTrace();

		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();

				}
			}

		}
		return userId;

	}

	public int findBySelectInfo(String loginId) {
		Connection conn = null;
		int userId = 0;
		try {
			conn = DBManager.getConnection();
			String sql = "select * from c_user where login_id=? ";
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, loginId);
			ResultSet rs = pStmt.executeQuery();
			if(!rs.next()) {
				return 0;
			}
			userId = rs.getInt("id");

		} catch (SQLException e) {
			e.printStackTrace();

		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();

				}
			}

		}
		return userId;

	}

	public int userAdd(String inputId, String inputPassword, String inputName, String inputBirthdate) {
		Connection conn = null;
		int result = 0;
		try {
			conn = DBManager.getConnection();
			String sql = "insert into c_user(name,login_id,password,birth_date,create_date,update_date) values (?,?,?,?,now(),now())";
			String passkey = passKey(inputPassword);
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, inputName);
			pStmt.setString(2, inputId);
			pStmt.setString(3, passkey);
			pStmt.setString(4, inputBirthdate);

			result = pStmt.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();

		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();

				}
			}

		}
		return result;
	}

	public UserDataBeans findByUserId(int userId) {
		Connection conn = null;
		UserDataBeans user = new UserDataBeans();
		try {
			String sql = "select * from c_user where id =?";
			conn = DBManager.getConnection();
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setInt(1, userId);
			ResultSet rs = pStmt.executeQuery();

			while (rs.next()) {
				user.setLoginId(rs.getString("login_id"));
				user.setName(rs.getString("name"));
				user.setPassword(rs.getString("password"));
				user.setId(rs.getInt("id"));
			}

		} catch (SQLException e) {
			e.printStackTrace();

		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();

				}
			}

		}
		return user;
	}

	public String passKey(String password) {

		String source = password;
		//ハッシュ生成前にバイト配列に置き換える際のCharset
		Charset charset = StandardCharsets.UTF_8;
		//ハッシュアルゴリズム
		String algorithm = "MD5";
		String result = null;
		//ハッシュ生成処理
		byte[] bytes = null;
		try {
			bytes = MessageDigest.getInstance(algorithm).digest(source.getBytes(charset));

			result = DatatypeConverter.printHexBinary(bytes);
			//標準出力
			System.out.println(result);

		} catch (NoSuchAlgorithmException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();

		}
		return result;
	}
}
