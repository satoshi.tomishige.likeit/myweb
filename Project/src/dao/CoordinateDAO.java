package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import base.DBManager;
import beans.CoordinateBeans;

public class CoordinateDAO {
	public static int CoordinationRegister(CoordinateBeans CB) {
		Connection conn = null;
		PreparedStatement pstmt = null;
		int result = 0;
		try {
			conn = DBManager.getConnection();
			String sql = "insert into c_coordinate (full_photo,upload_date,user_id)values(?,now(),?)";
			pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, CB.getPhoto());
			pstmt.setInt(2, CB.getUser_id());

			result = pstmt.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();

		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();

				}
			}

		}
		return result;
	}

	public int findNumber() {
		Connection conn = null;

		int result = 0;
		try {
			conn = DBManager.getConnection();
			Statement stmt = conn.createStatement();
			String sql = "select count(*) as id from c_coordinate;";
			ResultSet rs = stmt.executeQuery(sql);
			while (rs.next()) {
				result = rs.getInt("id");
			}
		} catch (SQLException e) {
			e.printStackTrace();

		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();

				}
			}

		}
		return result;
	}

	public List<CoordinateBeans> getList(int userId) {
		Connection conn = null;
		ArrayList<CoordinateBeans> CodeList = new ArrayList<CoordinateBeans>();
		// TODO 自動生成されたメソッド・スタブ
		try {
			conn = DBManager.getConnection();
			String sql = "select * from c_coordinate where user_id = ? ";
			PreparedStatement pstmt = conn.prepareStatement(sql);
			pstmt.setInt(1, userId);
			ResultSet rs = pstmt.executeQuery();

			while (rs.next()) {
				CoordinateBeans CB = new CoordinateBeans(
						rs.getInt("id"),
						rs.getString("full_photo"),
						rs.getInt("user_id"),
						rs.getString("upload_date"));
				CodeList.add(CB);

			}

		} catch (SQLException e) {
				e.printStackTrace();

			} finally {
				// データベース切断
				if (conn != null) {
					try {
						conn.close();
					} catch (SQLException e) {
						e.printStackTrace();

					}
				}

			}



		return CodeList;
	}
}
