package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import base.DBManager;
import beans.ItemBeans;

public class CoordinateDetailDAO {

	public int addItem(int coordinateId, String[] itemList) {

		Connection conn = null;
		int rs = 0;
		try {
			conn = DBManager.getConnection();

			for (String ItemId : itemList) {
				String sql = "insert into c_coordinate_detail(coordinate_id,item_id) values(?,?)";
				PreparedStatement pstmt = conn.prepareStatement(sql);
				pstmt.setInt(1, coordinateId);
				pstmt.setString(2, ItemId);
				rs = pstmt.executeUpdate();
			}
		} catch (SQLException e) {
			e.printStackTrace();

		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();

				}
			}

		}
		return rs;
	}

	public static ArrayList<ItemBeans> findItem(int codeId) {

		Connection conn = null;
		ArrayList<ItemBeans> itemList = new ArrayList<ItemBeans>();
		try {
			conn = DBManager.getConnection();
			String sql = "select * from c_coordinate_detail t1 inner join c_item t2 on t1.item_id=t2.id where coordinate_id = ?";
			PreparedStatement stmt = conn.prepareStatement(sql);
			stmt.setInt(1, codeId);
			ResultSet rs = stmt.executeQuery();

			while (rs.next()) {
				ItemBeans item = new ItemBeans(
						rs.getInt("item_id"),
						rs.getInt("class_id"),
						rs.getInt("color_id"),
						rs.getInt("formal"),
						rs.getString("brand"),
						rs.getString("store"),
						rs.getString("memo"),
						rs.getString("photo"),
						rs.getInt("user_id"),
						rs.getInt("price"));
				itemList.add(item);

			}
		} catch (SQLException e) {
			e.printStackTrace();

		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();

				}
			}

		}
		return itemList;
	}

	}
