package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import base.DBManager;
import beans.ItemClassBeans;

public class ItemClassDAO {
	public ArrayList<ItemClassBeans> findAllClass() {
		Connection conn = null;
		ArrayList<ItemClassBeans> list = new ArrayList<ItemClassBeans>();
		try {
			conn = DBManager.getConnection();

			Statement stmt = conn.createStatement();

			String sql = "select * from c_class";

			ResultSet rs = stmt.executeQuery(sql);

			while (rs.next()) {
				ItemClassBeans ICB = new ItemClassBeans();
				ICB.setId(rs.getInt("id"));
				ICB.setName(rs.getString("name"));
				list.add(ICB);
			}
		} catch (SQLException e) {
			e.printStackTrace();

		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();

				}
			}

		}
		return list;
	}

	public static ArrayList<ItemClassBeans> findById(String[] classList) {

		Connection conn = null;
		ArrayList<ItemClassBeans> list = new ArrayList<ItemClassBeans>();
		try {
			conn = DBManager.getConnection();
			String sql = "select * from c_class where id=?";

			for (int i = 0; i < classList.length; i++) {
				PreparedStatement pstmt = conn.prepareStatement(sql);
				pstmt.setString(1, classList[i]);
				ResultSet rs = pstmt.executeQuery();
				while(rs.next()) {
				ItemClassBeans ICB = new ItemClassBeans();
				ICB.setId(rs.getInt("id"));
				ICB.setName(rs.getString("name"));
				list.add(ICB);}
			}
		} catch (SQLException e) {
			e.printStackTrace();

		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();

				}
			}

		}
		return list;
	}
}
