package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.http.Part;

import beans.CoordinateBeans;
import beans.ItemBeans;
import beans.ItemClassBeans;
import beans.UserDataBeans;
import dao.CoordinateDAO;
import dao.ItemClassDAO;
import dao.ItemDAO;

/**
 * Servlet implementation class CodeRegister
 */
@WebServlet("/CodeRegister")
@MultipartConfig(location = "C:\\Users\\LIKEIT_STUDENT.DESKTOP-1P6BTJU.000\\Documents\\myweb\\Project\\WebContent\\img", maxFileSize = 1048576)
public class CodeRegister extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public CodeRegister() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		ItemClassDAO ICaD = new ItemClassDAO();
		List<ItemClassBeans> CaList = ICaD.findAllClass();

		request.setAttribute("classlist", CaList);
		RequestDispatcher dispatcher = request.getRequestDispatcher("WEB-INF/jsp/code_register.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		HttpSession session = request.getSession();
		int userId = ((UserDataBeans) session.getAttribute("userInfo")).getId();
		request.setCharacterEncoding("UTF-8");
		Part part = request.getPart("file");
		String name = this.getFileName(part);
		part.write(name);

		String[] classIdList = request.getParameterValues("class");
		List<ItemClassBeans> classList = new ArrayList<ItemClassBeans>();
		classList = ItemClassDAO.findById(classIdList);

		Map<String, ArrayList<ItemBeans>> itemMap = new HashMap();
		for (String classId : classIdList) {
			ArrayList<ItemBeans> list = ItemDAO.findMyItem(userId, Integer.parseInt(classId));
			itemMap.put(classId, list);
		}

		request.setAttribute("itemMap", itemMap);

		CoordinateBeans CB = new CoordinateBeans();
		CB.setPhoto(name);
		CB.setUser_id(userId);
		int result = CoordinateDAO.CoordinationRegister(CB);

		if (result != 1) {
			request.setAttribute("errMsg", "登録に失敗しました");
			RequestDispatcher dispatcher = request.getRequestDispatcher("WEB-INF/jsp/code_register.jsp");
			dispatcher.forward(request, response);
		} else {
			request.setAttribute("classList", classList);
			RequestDispatcher dispatcher = request.getRequestDispatcher("WEB-INF/jsp/code_register_detail.jsp");
			dispatcher.forward(request, response);
		}
	}

	private String getFileName(Part part) {
		String name = null;
		for (String dispotion : part.getHeader("Content-Disposition").split(";")) {
			if (dispotion.trim().startsWith("filename")) {
				name = dispotion.substring(dispotion.indexOf("=") + 1).replace("\"", "").trim();
				name = name.substring(name.lastIndexOf("\\") + 1);
				break;
			}
		}
		return name;
	}

}
