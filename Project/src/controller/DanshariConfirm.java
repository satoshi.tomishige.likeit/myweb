package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.ItemBeans;
import dao.ItemDAO;

/**
 * Servlet implementation class DanshariConfirm
 */
@WebServlet("/DanshariConfirm")
public class DanshariConfirm extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public DanshariConfirm() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		HttpSession session =request.getSession();
		if(session.getAttribute("userInfo")==null) {response.sendRedirect("Login");
	}else {
		int itemId =Integer.parseInt(request.getParameter("selectId"));

		ItemDAO ID = new ItemDAO();
		ItemBeans IB =ID.findItem(itemId);
		request.setAttribute("item", IB);
		RequestDispatcher dispatcher = request.getRequestDispatcher("WEB-INF/jsp/danshari_confirm.jsp");
		dispatcher.forward(request, response);
	}



	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		int itemId =Integer.parseInt(request.getParameter("selectId"));

		ItemDAO ID = new ItemDAO();
		int result  = ID.remove(itemId);
		if(result!=1) {
			request.setAttribute("errMsg", "削除に失敗しました");
			RequestDispatcher dispatcher = request.getRequestDispatcher("WEB-INF/jsp/danshari_confirm.jsp");
			dispatcher.forward(request, response);
		}else {
			request.setAttribute("Msg", "DANSHARIしました");
			response.sendRedirect("Danshari");
		}
	}

}
