package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.UserDAO;

/**
 * Servlet implementation class UserRegister
 */
@WebServlet("/UserRegister")
public class UserRegister extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public UserRegister() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		RequestDispatcher dispatcher = request.getRequestDispatcher("WEB-INF/jsp/user_register.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");

		String inputId = request.getParameter("inputId");
		String inputPassword = request.getParameter("inputPassword");
		String inputName = request.getParameter("inputName");
		String inputBirthdate = request.getParameter("inputBirthdate");
		String confirmPassword = request.getParameter("confirmPassword");

		if (!(inputPassword.equals(confirmPassword))) {
			request.setAttribute("errMsgPass", "同じ文字を入力してください");
			doGet(request, response);
		} else {

			UserDAO userdao = new UserDAO();
			if (userdao.findBySelectInfo(inputId) != 0) {
				request.setAttribute("errMsgId", "同じIDが存在します");
				doGet(request, response);
			} else {

				int result = userdao.userAdd(inputId, inputPassword, inputName, inputBirthdate);

				if (result == 1) {
					response.sendRedirect("Login");
				} else {
					request.setAttribute("errMsg", "入力された内容は正しくありません");
					doGet(request, response);
				}
			}

		}

	}

}
