package controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.http.Part;

import beans.ItemBeans;
import beans.ItemClassBeans;
import beans.ItemColorBeans;
import beans.UserDataBeans;
import dao.ItemClassDAO;
import dao.ItemColorDAO;
import dao.ItemDAO;

/**
 * Servlet implementation class ItemRegister
 */
@WebServlet("/ItemRegister")
@MultipartConfig(location="C:\\Users\\LIKEIT_STUDENT.DESKTOP-1P6BTJU.000\\Documents\\myweb\\Project\\WebContent\\img", maxFileSize=1048576)
public class ItemRegister extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public ItemRegister() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		ItemColorDAO ICoD =new ItemColorDAO();
		ItemClassDAO ICaD =new ItemClassDAO();
		List<ItemColorBeans> Colist = ICoD.findAllColor();
		List<ItemClassBeans> Calist = ICaD.findAllClass();

		request.setAttribute("colorlist",Colist );
		request.setAttribute("classlist", Calist);
		RequestDispatcher dispatcher = request.getRequestDispatcher("WEB-INF/jsp/item_register.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		 request.setCharacterEncoding("UTF-8");
		Part part = request.getPart("file");
        String name = this.getFileName(part);
        part.write(name);

		int color = Integer.parseInt(request.getParameter("color"));
		int price = Integer.parseInt(request.getParameter("price"));
		int classId =Integer.parseInt(request.getParameter("class"));
		int formal = Integer.parseInt(request.getParameter("formal"));
		String brand = request.getParameter("brand");
		String store = request.getParameter("store");
		String memo = request.getParameter("memo");
		int userId = ((UserDataBeans)session.getAttribute("userInfo")).getId();
		ItemBeans IB = new ItemBeans();
		IB.setClasssId(classId);
		IB.setColorId(color);
		IB.setFormal(formal);
		IB.setBrand(brand);
		IB.setStore(store);
		IB.setMemo(memo);
		IB.setPhoto(name);
		IB.setUserId(userId);
		IB.setPrice(price);
		int result = ItemDAO.ItemRegister(IB);

		if(result!=1) {
			request.setAttribute("errMsg", "登録に失敗しました");
			RequestDispatcher dispatcher = request.getRequestDispatcher("WEB_INF/jsp/item_register.jsp");
			dispatcher.forward(request, response);
		}else {
			response.sendRedirect("ItemList");
		}
	}
	 private String getFileName(Part part) {
	        String name = null;
	        for (String dispotion : part.getHeader("Content-Disposition").split(";")) {
	            if (dispotion.trim().startsWith("filename")) {
	                name = dispotion.substring(dispotion.indexOf("=") + 1).replace("\"", "").trim();
	                name = name.substring(name.lastIndexOf("\\") + 1);
	                break;
	            }
	        }
	        return name;
	    }
}
