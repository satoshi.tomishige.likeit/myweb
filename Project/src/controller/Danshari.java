package controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.ItemBeans;
import beans.UserDataBeans;
import dao.ItemDAO;

/**
 * Servlet implementation class Danshari
 */
@WebServlet("/Danshari")
public class Danshari extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public Danshari() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		HttpSession session =request.getSession();
		if(session.getAttribute("userInfo")==null) {response.sendRedirect("Login");
	}else {
		int userId = ((UserDataBeans)session.getAttribute("userInfo")).getId();
		ItemDAO itemdao = new ItemDAO();
		List<ItemBeans> list = itemdao.findSortedItem(userId);
		request.setAttribute("ItemList", list);
		RequestDispatcher dispatcher = request.getRequestDispatcher("WEB-INF/jsp/danshari.jsp");
		dispatcher.forward(request, response);}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
