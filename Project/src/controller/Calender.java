package controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.CoordinateBeans;
import beans.UserDataBeans;
import dao.CoordinateDAO;

/**
 * Servlet implementation class Calender
 */
@WebServlet("/Calender")
public class Calender extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public Calender() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		int userId = ((UserDataBeans)session.getAttribute("userInfo")).getId();
		CoordinateDAO CD = new CoordinateDAO();
		List<CoordinateBeans> CL = CD.getList(userId);

//		ArrayList<String> idList = new ArrayList<String>();
//        for (CoordinateBeans codebean : CL) {
//            idList.add(codebean.getId());
//        }


//		Map<String, ArrayList<ItemBeans>> codeMap = new HashMap();
//		for (String codeId : idList ) {
//			ArrayList<ItemBeans> list = CoordinateDetailDAO.findItem(userId, Integer.parseInt(codeId));
//			codeMap.put(codeId, list);}
//		request.setAttribute("codeMap", codeMap);


		request.setAttribute("codeList", CL);
		RequestDispatcher dispatcher = request.getRequestDispatcher("WEB-INF/jsp/code_calender.jsp");
		dispatcher.forward(request, response);}


	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
