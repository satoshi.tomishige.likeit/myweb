package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.CoordinateDAO;
import dao.CoordinateDetailDAO;
import dao.ItemDAO;

/**
 * Servlet implementation class CodeDetailRegister
 */
@WebServlet("/CodeDetailRegister")
public class CodeDetailRegister extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public CodeDetailRegister() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String[] itemList = request.getParameterValues("item");
		CoordinateDAO CD = new CoordinateDAO();
		int CoordinateId = CD.findNumber();
		CoordinateDetailDAO CDD = new CoordinateDetailDAO();
		int rs=CDD.addItem(CoordinateId,itemList);
		ItemDAO ID = new ItemDAO();
		int Urs = ID.addUse(itemList);
		if(rs!=1) {
			request.setAttribute("errMsg", "登録に失敗しました");
			RequestDispatcher dispatcher = request.getRequestDispatcher("WEB-INF/jsp/code_register_detail.jsp");
		dispatcher.forward(request, response);
	} else {
		response.sendRedirect("Calender");
	}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
