<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>Today Coordination</title>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link href="css/style.css" rel="stylesheet" type="text/css" />
</head>
	<body><ul>

        <li><a href="Main"><h4>#COORDINATOR</h4></a></li>
        <li><a href="CodeRegister"><h4>TODAY</h4></a></li>
        <li><a href="ItemRegister"><h4>NEWITEM</h4></a></li>
        <li><a href="Calender"><h4>CALENDER</h4></a></li>
        <li><a href="ItemList"><h4>MYLIST</h4></a></li>
        <lir><a href="Logout"><h4>LOGOUT</h4></a></lir>
        <lir><a href="Danshari"><h4>DANSHARI</h4></a></lir>
        <lir><a href="UserInfo"><h4>${userInfo.name }</h4></a></lir>
        </ul>

        <br>
        <br>
        <div class="container"><div class="row"> <div class="col-sm-12"> <h1 align=center>TODAY'S COORDINATION</h1></div></div>
        <br>
            <form action="CodeRegister" method="post" enctype="multipart/form-data">

             <div class="form-group" align=center>

                                <label for="exampleInputImage">Image</label><br>
                  <div class="row"><div class="col-sm-12" ><input type="file" name="file"><br><br><br>
                  </div>
              </div>
            </div>
                <div class="form-group" align=center>
                <label for="exampleInputClass">Classification</label><br>
                                <div class="row">
                                <c:forEach var="ItemClassBeans" items="${classlist }">
                                    <div class="col-sm-3">
                                        <input type="checkbox" name="class" value="${ItemClassBeans.id }" >${ItemClassBeans.name }
                                        </div></c:forEach>

                                    <div class="col-sm-12">
                                        <button type="submit" class="btn btn-dark" >Next</button></div>
                                </div>
                            </div>


            </form>
        </div>
    </body>
</html>