
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>New Item</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
<link href="css/style.css" rel="stylesheet" type="text/css" />
</head>
<body>
	<ul>

		<li><a href="Main"><h4>#COORDINATOR</h4></a></li>
		<li><a href="CodeRegister"><h4>TODAY</h4></a></li>
		<li><a href="ItemRegister"><h4>NEWITEM</h4></a></li>
		<li><a href="Calender"><h4>CALENDER</h4></a></li>
		<li><a href="ItemList"><h4>MYLIST</h4></a></li>
		<lir>
		<a href="Logout"><h4>LOGOUT</h4></a></lir>
		<lir>
		<a href="Danshari"><h4>DANSHARI</h4></a></lir>
		<lir>
		<a href="UserInfo"><h4>${userInfo.name }</h4></a></lir>
	</ul>


	<div class="container">
		<br> <br>
		<h1 align=center>NEW ITEM</h1>
		<form action="ItemRegister" method="post" enctype="multipart/form-data">
			<div class="form-group" align=center>
				<label for="exampleInputImage">Image</label><br> <input
					type="file" name="file">
			</div>
			<div class="form-group" align=center>
				<label for="exampleInputBrand">Classification</label><br>
				<div class="row">
				<c:forEach var="ItemClassBeans" items="${classlist }">
					<div class="col-sm-3">
						<input type="radio" name="class" value="${ItemClassBeans.id }" >${ItemClassBeans.name }
					</div></c:forEach>
				</div>
			</div>
			<br>
			<div class="form-group block-center" align=center>
				<label for="exampleInputPrice">Price</label> <input type="number"
					class="form-control" id="exampleInputPrice"
					placeholder="Enter Price" name="price">
			<!-- 		<input type="range" min="1000"
					max="100000" step="1000" class="form-control"
					id="exampleInputPrice" placeholder="Enter Price">
 -->
			</div>
			<div class="form-group" align=center>
				<label for="exampleInputColor">Color</label>
				<div class="row">
				<c:forEach var="ItemColorBeans" items="${colorlist }">
					<div class="col-sm-3">
						<input type="radio" name="color" value="${ItemColorBeans.id }" ><font
							color="${ItemColorBeans.code }">■</font>${ ItemColorBeans.name }
					</div></c:forEach>
				</div>
			</div>
			<div class="form-group block-center">
				<label for="exampleInputFormal">Formal</label> <input type="range"
					min="1" max="5" step="1" class="form-control"
					id="exampleInputFormal" placeholder="Enter Formal" name="formal">

			</div>


			<div class="form-group">
				<label for="exampleInputBrand">Brand</label> <input type="text"
					class="form-control" id="exampleInputBrand"
					placeholder="Enter Brand" name="brand">
			</div>
			<div class="form-group">
				<label for="exampleInputStore">Store</label> <input type="text"
					class="form-control" id="exampleInputStore" name="store" placeholder="Enter Store">
			</div>
			<div class="form-group">
				<label for="exampleInputMemo">Memo</label> <input type="text"
					class="form-control" id="exampleInputMemo" name="memo" placeholder="Enter Memo">
			</div>

			<br>
			<div class="button_wrapper">
				<button class="btn btn-dark" type="submit" role="button"
					align="center">GET</button>
			</div>

		</form>
	</div>
</body>
</html>