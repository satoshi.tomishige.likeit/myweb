<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>LOGIN</title>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link href="css/style.css" rel="stylesheet" type="text/css" />
</head>
	<body><ul>
        <li><a href="Login"><h4>#COORDINATOR</h4></a></li>
        </ul>

        <div class="container">
            <br>
        <h1 align="center">COORDINATOR</h1>
            <h2 align="center">LOGIN</h2>
            <c:if test="${ errMsg!=null}"><div class="alert alert-danger" role="alert">
		  ${errMsg }
		</div></c:if>
            <form class="form-signin" action="Login" method="post">
                <div class="form-group">
    <label for="exampleInputId">LOGIN ID</label>
    <input type="text" class="form-control" id="exampleInputId" name="loginId"  placeholder="Enter ID">
  </div>
                  <div class="form-group">
    <label for="exampleInputPassword">PASSWORD</label>
    <input type="password" class="form-control" id="exampleInputPsaaword" name="password" placeholder="Enter Password">
  </div>
                <br><div class="button_wrapper"><button class="btn btn-dark" type="submit" role="button" align="center">LOGIN</button>
                </div>
            </form>
            <br>
            <br>
            <div class="button_wrapper"><a href="UserRegister" type="button" class="btn btn-dark">NEWUSER</a>
                </div>
        </div>
    </body>
</html>