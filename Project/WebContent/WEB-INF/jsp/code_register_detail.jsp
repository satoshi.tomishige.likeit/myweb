<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>Today Coordination</title>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link href="css/style.css" rel="stylesheet" type="text/css" />
     <link href="css/tab_style.css" rel="stylesheet" type="text/css" />
</head>
	<body><ul>

        <li><a href="Main"><h4>#COORDINATOR</h4></a></li>
        <li><a href="CodeRegister"><h4>TODAY</h4></a></li>
        <li><a href="ItemRegister"><h4>NEWITEM</h4></a></li>
        <li><a href="Calender"><h4>CALENDER</h4></a></li>
        <li><a href="ItemList"><h4>MYLIST</h4></a></li>
        <lir><a href="Logout"><h4>LOGOUT</h4></a></lir>
        <lir><a href="Danshari"><h4>DANSHARI</h4></a></lir>
        <lir><a href="UserInfo"><h4>${userInfo.name }</h4></a></lir>
        </ul>

        <br>
        <br>
        <div class="container"><div class="row"> <div class="col-sm-12"> <h1 align=center>TODAY'S COORDINATION</h1></div></div>


        <br><form action="CodeDetailRegister"   enctype="multipart/form-data">
            <div class="tab_wrap">


            <c:set var="Ftrue" value="true"/>
            <c:forEach var="ItemClassBeans" items="${classList }">
                <c:if test="${Ftrue=true }">
            	<input id="tab${ItemClassBeans.id }" type="radio" name="tab_btn" checked >
            	</c:if>
            	<c:if test="${Ftrue=false }">
            	<input id="tab${ItemClassBeans.id }" type="radio" name="tab_btn">
            	</c:if>
            	<c:set var="Ftrue" value="false"/>
            </c:forEach>


	<div class="tab_area">
		<c:forEach var="ItemClassBeans" items="${classList }">
			<label class="tab${ItemClassBeans.id }_label" for="tab${ItemClassBeans.id }">${ItemClassBeans.name }</label>
		</c:forEach>
	</div>

	<div class="panel_area">
		<c:forEach var="classBeans" items="${itemMap }">
			<div id="panel${classBeans.key }" class="tab_panel">
				<div class="row">
					<c:forEach var="ItemBeans" items="${classBeans.value }">
					<div class="col-sm-4">
					 <img src="img/${ItemBeans.photo }" ><br><input type="checkbox" name="item" value="${ItemBeans.id }"></image></div></c:forEach>
	            </div>
			</div>
		</c:forEach>

	</div>
</div>
            <div align=center><button type="submit" class="btn btn-dark">NEXT</button></div>
            </form>
        </div>
    </body>
</html>