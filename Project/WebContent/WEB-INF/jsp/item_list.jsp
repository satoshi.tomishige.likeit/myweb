<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>MY LIST</title>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link href="css/style.css" rel="stylesheet" type="text/css" />
</head>
	<body><ul>

        <li><a href="Main"><h4>#COORDINATOR</h4></a></li>
        <li><a href="CodeRegister"><h4>TODAY</h4></a></li>
        <li><a href="ItemRegister"><h4>NEWITEM</h4></a></li>
        <li><a href="Calender"><h4>CALENDER</h4></a></li>
        <li><a href="ItemList"><h4>MYLIST</h4></a></li>
        <lir><a href="Logout"><h4>LOGOUT</h4></a></lir>
        <lir><a href="Danshari"><h4>DANSHARI</h4></a></lir>
        <lir><a href="UserInfo"><h4>${userInfo.name }</h4></a></lir>
        </ul>

        <br>
        <br>

        <div class="container"><div class="row"> <div class="col s12"> <h1 align=center>MY LIST</h1></div></div>
        <br>
         <div class="row">
	<c:forEach var="ItemBeans" items="${ItemList}">
				<div class="col-sm-3">
					<div class="card" align=center>
						<div class="card-image">
							<a href="ItemInfo?selectId=${ItemBeans.id }"><img src="img/${ItemBeans.photo }"></a>
						</div>

						<div class="card-content">
							<span class="card-title"><c:if test="${ItemBeans.brand !=null}"><h6>#${ItemBeans.brand }</h6></c:if>
							<c:if test="${ItemBeans.store !=null }"><h6>#${ItemBeans.store }</h6></c:if>
							</span>
							<p>${ItemBeans.price }円
							</p>
						</div>
					</div>
				</div></c:forEach>


        </div>
        </div>
    </body>
</html>