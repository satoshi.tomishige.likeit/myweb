<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>Item</title>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link href="css/style.css" rel="stylesheet" type="text/css" />
</head>
	<body><ul>

        <li><a href="Main"><h4>#COORDINATOR</h4></a></li>
        <li><a href="CodeRegister"><h4>TODAY</h4></a></li>
        <li><a href="ItemRegister"><h4>NEWITEM</h4></a></li>
        <li><a href="Calender"><h4>CALENDER</h4></a></li>
        <li><a href="ItemList"><h4>MYLIST</h4></a></li>
        <lir><a href="Logout"><h4>LOGOUT</h4></a></lir>
        <lir><a href="Danshari"><h4>DANSHARI</h4></a></lir>
        <lir><a href="UserInfo"><h4>${userInfo.name }</h4></a></lir>
        </ul>

        <div class="container">
        <br><br>
            <div class="row">

                <div class="col-sm-6" align=center><img src="img/${item.photo }"  >
                </div>
                <div class="col-sm-6" align=left>
                <br><br><br>

                    　　　#${item.price }円<br>
                    　　　<c:if test="${item.brand != null}" >#${item.brand }<br></c:if>
                    　　　<c:if test="${item.store != null}" >#${item.store }<br></c:if>
                    　　　<c:if test="${item.memo != null}" >#${item.memo }<br></c:if>
                    <br><br>
                    <div class="row">

                <div class="col-sm-3"><a href="DanshariConfirm?selectId=${item.id }" type="button" class="btn btn-dark">DANSHARI</a></div>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>