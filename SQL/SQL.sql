CREATE DATABASE co_db DEFAULT CHARACTER SET utf8;
use co_db;
create table c_user(id int primary key auto_increment
,name varchar(30)not null, 
login_id varchar(30)not null unique,
password varchar(256)not null,
birth_date date not null,
create_date date not null,
update_date date not null);
create table c_item(id int primary key auto_increment,
class_id int not null,
color_id int not null,
formal int not null,
brand varchar(30),
store varchar(30),
memo text,
photo varchar(256) not null,
user_id int not null);
create table c_class(id int auto_increment primary key,name varchar(256) not null);
create table c_color(id int auto_increment primary key,name varchar(256) not null,code varchar(256) not null);
create table c_coordinate(id int auto_increment primary key,full_photo varchar(256),upload_date date not null,user_id int not null);
create table c_coordinate_detail(id int auto_increment primary key,coordinate_id int not null,class_id int not null,item_id int not null);
insert into c_color(name,code) values ('White','#fdf5e6');
insert into c_color(name,code)  values ('Black','#000000');
insert into c_color(name,code)  values ('Grey','#d3d3d3');
insert into c_color(name,code)  values ('Brown','#d2691e');
insert into c_color(name,code)  values ('Beige','#f5f5dc');
insert into c_color(name,code)  values ('Green','#90ee90');
insert into c_color(name,code)  values ('Blue','#87cefa');
insert into c_color(name,code)  values ('Purple','#9370db');
insert into c_color(name,code)  values ('Yellow','#ffff00');
insert into c_color(name,code)  values ('Pink','#ff69b4');
insert into c_color(name,code)  values ('Red','#ff6347');
insert into c_color(name,code)  values ('Orange','#ffa50');
insert into c_color(name,code)  values ('Silver','#c0c0c0');
insert into c_color(name,code)  values ('Gold','#ffd700');
insert into c_color(name,code)  values ('Other','#ffffff');
insert into c_class(name) values ('TOPS');
insert into c_class(name) values ('JACKET/OUTER');
insert into c_class(name) values ('PANTS');
insert into c_class(name) values ('ALL IN ONE');
insert into c_class(name) values ('SKIRT');
insert into c_class(name) values ('ONEPIECE');
insert into c_class(name) values ('SUIT');
insert into c_class(name) values ('SHOOSE');
insert into c_class(name) values ('ACCESSORY');
insert into c_class(name) values ('BAG');


